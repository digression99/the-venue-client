import React, { Component } from 'react';
import styled from 'styled-components';
import { Element } from 'react-scroll';

import {
    Header,
    Featured,
    VenueInfo,
    Highlights,
    Pricing,
    Location,
    Footer
} from './components/organisms';

const Wrapper = styled.div`
  height : 2000px;
  background : cornflowerblue;
`;

class App extends Component {
    render() {
        return (
            <Wrapper>
                <Header />
                <Element
                    name="featured"
                >
                    <Featured />
                </Element>
                <Element
                    name="venueInfo"
                >
                    <VenueInfo />
                </Element>
                <Element
                    name="highlights"
                >
                    <Highlights />
                </Element>
                <Element
                    name="pricing"
                >
                    <Pricing />
                </Element>
                <Element
                    name="location"
                >
                    <Location />
                </Element>
                <Footer />
            </Wrapper>
        );
    }
}

export default App;
