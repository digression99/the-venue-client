import React, {Component} from 'react';
import { CountdownSlide } from '../components/molecules';

const SECOND_TIME = 1000;
const MINUTE_TIME = SECOND_TIME * 60;
const HOUR_TIME = MINUTE_TIME * 60;
const DAY_TIME = HOUR_TIME * 24;

class CountdownContainer extends Component {

    state = {
        deadline : 'Dec, 16, 2018',
        days : '0',
        hours : '0',
        minutes : '0',
        seconds : '0'
    };

    getTimeUntil() {
        const { deadline } = this.state;
        let time = Date.parse(deadline) - Date.parse(new Date());

        if (time < 0) return;

        const days = Math.floor(time / DAY_TIME);
        time -= days * DAY_TIME;
        const hours = Math.floor(time / HOUR_TIME);
        time -= hours * HOUR_TIME;
        const minutes = Math.floor(time / MINUTE_TIME);
        time -= minutes * MINUTE_TIME;
        const seconds = Math.floor(time /
SECOND_TIME);

        this.setState({
            days, hours, minutes, seconds
        });
    }

    componentDidMount() {
        setInterval(() => this.getTimeUntil(this.state.deadline), 1000);
    }

    render() {
        return (
            // countdown slide component 로 뺴기.
            <CountdownSlide
                {...this.state}
            />
        );
    }
}

export default CountdownContainer;
