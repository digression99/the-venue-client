import React from 'react';
import Zoom from 'react-reveal/Zoom';

export default ({ duration, delay, children }) => (
    <Zoom
        duration={duration}
        delay={delay}
    >
        {children}
    </Zoom>
);