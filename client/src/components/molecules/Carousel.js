import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';
import classNames from 'classnames/bind';

import { CarouselImage } from '../atoms';
import styles from '../../resources/styles.css';

import img1 from '../../resources/images/slide_one.jpg';
import img2 from '../../resources/images/slide_two.jpg';
import img3 from '../../resources/images/slide_three.jpg';

const cx = classNames.bind(styles);

const CarouselWrapper = styled.div`
  height : ${window.innerHeight}px;
  overflow : hidden;
`;

const Carousel = () => {

    const settings = {
        dots : false,
        infinite : true,
        autoplay : true,
        speed : 500
    };

    return (
        <CarouselWrapper className={cx("carrousel_wrapper")}>
            <Slider {...settings}>
                <CarouselImage img={img1} />
                <CarouselImage img={img2} />
                <CarouselImage img={img3} />
            </Slider>
        </CarouselWrapper>
    );
};

export default Carousel;
