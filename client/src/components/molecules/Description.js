import React from 'react';
import Fade from 'react-reveal/Fade';

import classNames from 'classnames/bind';
import styles  from '../../resources/styles.css';

const cx = classNames.bind(styles);

const Description = () => {
    return (
        <Fade>
            <div className={cx('center_wrapper')}>
                <h2>Highlights</h2>
                <div className={cx('highlight_description')}>
                    Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Adipisci dicta
                    distinctio dolore, dolorem exercitationem harum incidunt, ipsam
                    libero nihil obcaecati odio quae quam quis sapiente, sit
                    tenetur unde voluptate voluptatum?
                </div>
            </div>
        </Fade>
    );
};

export default Description;
