import React, {Component} from 'react';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';
import classNames from 'classnames/bind';

import { Button } from '../atoms';
import styles from '../../resources/styles.css';
const cx = classNames.bind(styles);

class Discount extends Component {

    state = {
        discountStart : 0,
        discountEnd : 30
    };

    changePercentage() {
        const { discountStart, discountEnd } = this.state;
        if (discountStart < discountEnd) {
            this.setState({
                discountStart : discountStart + 1
            });
        }
    };

    componentDidUpdate() {
        setTimeout(() => {
            this.changePercentage();
        }, 50);
    }

    render() {
        return (
            <div className={cx('center_wrapper')}>
                <div className={cx('discount_wrapper')}>
                    <Fade
                        duration={1500}
                        onReveal={this.changePercentage.bind(this)}
                    >
                        <div className={cx('discount_percentage')}>
                            <span>{this.state.discountStart}%</span>
                            <span>off</span>
                        </div>
                    </Fade>

                    <Slide
                        right
                        duration={1500}
                    >
                        <div className={cx('discount_description')}>
                            <h3>Purchase ticket before 20th June.</h3>
                            <p>Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Aut delectus
                                eligendi modi reiciendis suscipit! Consectetur
                                debitis dicta dolor excepturi incidunt magni,
                                rerum tempora. Ad at distinctio magnam,
                                quis tempore voluptas!</p>
                            <Button
                                bck="#ffa800"
                                txtcolor="#fff"
                                link=""
                            >Purchase ticket</Button>
                        </div>
                    </Slide>
                </div>
            </div>
        );
    }
}

export default Discount;
