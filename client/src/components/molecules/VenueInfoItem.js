import React from 'react';
import classNames from 'classnames/bind';
import Zoom from 'react-reveal/Zoom';

import styles from '../../resources/styles.css';
import {Icon} from '../atoms';
// import withZoom from '../hoc/withZoom';

const cx = classNames.bind(styles);

// const VenueInfoItem = ({
//                            iconUrl,
//                            squareColor,
//                            title,
//                            content,
//                            // zoomDelay,
//                            // zoomDuration
//                        }) => (
//     <div className={cx('vn_item')}>
//         <div className={cx('vn_outer')}>
//             <div className={cx('vn_inner')}>
//                 <div className={cx('vn_icon_square', squareColor)}/>
//                 <Icon
//                     className={cx('vn_icon')}
//                     iconUrl={iconUrl}
//                 >
//                 </Icon>
//                 <div
//                     className={cx('vn_title')}
//                 >
//                     {title}
//                 </div>
//                 <div
//                     className={cx('vn_desc')}
//                 >
//                     {content}
//                 </div>
//             </div>
//         </div>
//     </div>
// );
//
// export default withZoom(VenueInfoItem);

export default ({
                    iconUrl,
                    squareColor,
                    title,
                    content,
                    zoomDelay,
                    zoomDuration
                }) => (
    <Zoom
        duration={zoomDuration}
        delay={zoomDelay}
    >
        <div className={cx('vn_item')}>
            <div className={cx('vn_outer')}>
                <div className={cx('vn_inner')}>
                    <div className={cx('vn_icon_square', squareColor)}/>
                    <Icon
                        className={cx('vn_icon')}
                        iconUrl={iconUrl}
                    >
                    </Icon>
                    <div
                        className={cx('vn_title')}
                    >
                        {title}
                    </div>
                    <div
                        className={cx('vn_desc')}
                    >
                        {content}
                    </div>
                </div>
            </div>
        </div>
    </Zoom>
);

