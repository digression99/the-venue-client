import React from 'react';
import Slide from 'react-reveal/Slide';
import classNames from 'classnames/bind';

import { CountdownItem } from '../atoms';
import styles from '../../resources/styles.css';

const cx = classNames.bind(styles);

const CountdownSlide = ({ days, hours, minutes, seconds }) => {
    return (
        <Slide
            left
            duration={1500}
            delay={500}
        >
            <div className={cx('countdown_wrapper')}>
                <div className={cx('countdown_top')}>
                    Event starts in
                </div>
                <div className={cx('countdown_bottom')}>
                    <CountdownItem
                        figure={days}
                        title="Days"
                    />
                    <CountdownItem
                        figure={hours}
                        title="Days"
                    />
                    <CountdownItem
                        figure={minutes}
                        title="Min"
                    />
                    <CountdownItem
                        figure={seconds}
                        title="Sec"
                    />
                </div>
            </div>
        </Slide>
    );
};

export default CountdownSlide;
