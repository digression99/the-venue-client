import React from 'react';
import styled from 'styled-components';
import classNames from 'classnames/bind';

import styles from '../../resources/styles.css';
const cx = classNames.bind(styles);

const CarouselImage = styled.div`
  background-image : url(${({ img }) => img });
  height : ${window.innerHeight}px;
`;

export default ({ img }) => (
    <CarouselImage
        className={cx('carrousel_image')}
        img={img}
    />
);