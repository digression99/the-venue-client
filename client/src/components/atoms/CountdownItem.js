import React from 'react';
import classNames from 'classnames/bind';
import styles from '../../resources/styles.css';

const cx = classNames.bind(styles);

const CountdownItem = ({ figure, title }) => {
    return (
        <div className={cx('countdown_item')}>
            <div className={cx('countdown_time')}>
                {figure}
            </div>
            <div className={cx('countdown_tag')}>
                {title}
            </div>
        </div>
    );
};

export default CountdownItem;
