import React, {Fragment} from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import classNames from 'classnames/bind';

import TicketIcon from '../../resources/images/icons/ticket.png';

const StyledButton = styled(Button)`
  && { // problems with material-ui. This is hack to solve it.
      background : ${props => props.bck ? props.bck : 'red'};
      color : ${props => props.txtcolor ? props.txtcolor : 'white'};
      font-weight : bolder;
      
      &:hover {
        background-color : red;
      }
  }
`;

export default ({ link, bck, txtcolor, children}) => {
    return (
        <StyledButton
            variant="contained"
            size="small"
            bck={bck}
            txtcolor={txtcolor}
            href={link}
        >
            <img src={TicketIcon}
                 alt="icon_button"
                 className="iconImage"
            />
            {children}
        </StyledButton>
    );
};
