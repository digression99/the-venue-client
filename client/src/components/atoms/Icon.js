import styled from "styled-components";

export default styled.div`
  background : url(${({ iconUrl }) => iconUrl });
`;

