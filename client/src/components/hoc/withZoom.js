import React, { Component } from 'react';
import Zoom from 'react-reveal/Zoom';

export default WrappedComponent => {
    return class PP extends Component {
        render() {
            return (
                <Zoom
                    duration={this.props.zoomDuration}
                    delay={this.props.zoomDelay}
                >
                    <WrappedComponent { ...this.props }/>
                </Zoom>
            );
        }
    }
}