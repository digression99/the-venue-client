import React from 'react';
import styled from 'styled-components';

import Carousel from '../molecules/Carousel';
import { CountdownContainer } from '../../containers';

const Wrapper = styled.div`
  position : relative;
`;

const Featured = () => {
    return (
        <Wrapper>
            <Carousel />
            <div className="artist_name">
                <div className="wrapper">
                    Ariana Grande
                </div>
            </div>
            <CountdownContainer />
        </Wrapper>
    );
};

export default Featured;
