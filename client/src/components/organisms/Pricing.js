import React, {Component} from 'react';
import classNames from 'classnames/bind';
import Zoom from 'react-reveal/Zoom';

import { Button } from '../atoms';
import styles from '../../resources/styles.css';

const cx = classNames.bind(styles);

class Pricing extends Component {

    state = {
        prices : [100, 150, 250],
        positions : ['Balcony', 'Medium', 'Star'],
        descriptions : [
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur aut beatae, culpa eaque enim laborum magni nisi qui quia sit voluptates! Accusantium autem fugiat, fugit ipsum nam odit voluptates!',
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur aut beatae, culpa eaque enim laborum magni nisi qui quia sit voluptates! Accusantium autem fugiat, fugit ipsum nam odit voluptates!',
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur aut beatae, culpa eaque enim laborum magni nisi qui quia sit voluptates! Accusantium autem fugiat, fugit ipsum nam odit voluptates!'
        ],
        links : ['#', '#', '#'],
        revealDelays : [400, 100, 800]
    };

    renderPriceBoxes() {
        const ret = [];
        const {
            prices,
            positions,
            descriptions,
            links,
            revealDelays
        } = this.state;
        for (let i = 0; i < 3; ++i) {
            ret.push(
                <Zoom
                    key={i}
                    delay={revealDelays[i]}
                >
                    <div className={cx('pricing_item')}>
                        <div className={cx('pricing_inner_wrapper')}>
                            <div className={cx('pricing_title')}>
                                <span>${prices[i]}</span>
                                <span>{positions[i]}</span>
                            </div>
                            <div className={cx('pricing_description')}>
                                {descriptions[i]}
                            </div>
                            <div className={cx('pricing_buttons')}>
                                <Button
                                    bck="#ffa800"
                                    txtcolor="#fff"
                                    link={links[i]}
                                >Purchase</Button>
                            </div>
                        </div>
                    </div>
                </Zoom>
            );
        }
        return ret;
    }

    render() {
        return (
            <div className={cx('bck_black')}>
                <div className={cx('center_wrapper', 'pricing_section')}>
                    <h2>Pricing</h2>
                    <div className={cx('pricing_wrapper')}>
                        {this.renderPriceBoxes()}
                    </div>
                </div>
            </div>
        );
    }
}

export default Pricing;
