import React from 'react';
import classNames from 'classnames/bind';

import styles from '../../resources/styles.css';

import icon_calendar from '../../resources/images/icons/calendar.png';
import icon_location from '../../resources/images/icons/location.png';
import { VenueInfoItem } from '../molecules';
import ZoomContainer from '../../containers/ZoomContainer';

const cx = classNames.bind(styles);

export default () => (
    <div className={cx('bck_black')}>
        <div className={cx('center_wrapper')}>
            <div className={cx('vn_wrapper')}>
                {/*<ZoomContainer*/}
                    {/*duration={500}*/}
                    {/*delay={500}*/}
                {/*>*/}
                    {/*<VenueInfoItem*/}
                        {/*squareColor='bck_red'*/}
                        {/*iconUrl={icon_calendar}*/}
                        {/*title="Event Date & Time"*/}
                        {/*content="7 August 2017 @10.00pm"*/}
                    {/*/>*/}
                {/*</ZoomContainer>*/}
                {/*<ZoomContainer*/}
                    {/*zoomDuration={500}*/}
                    {/*zoomDelay={1000}*/}
                {/*>*/}
                    {/*<VenueInfoItem*/}
                        {/*squareColor='bck_yellow'*/}
                        {/*iconUrl={icon_location}*/}
                        {/*title="Event Location"*/}
                        {/*content="345 Speet Street Oakland, CA 9835"*/}
                    {/*/>*/}
                {/*</ZoomContainer>*/}
                <VenueInfoItem
                    squareColor='bck_red'
                    iconUrl={icon_calendar}
                    title="Event Date & Time"
                    content="7 August 2017 @10.00pm"
                    zoomDuration={500}
                    zoomDelay={500}
                />
                <VenueInfoItem
                    squareColor='bck_yellow'
                    iconUrl={icon_location}
                    title="Event Location"
                    content="345 Speet Street Oakland, CA 9835"
                    zoomDuration={500}
                    zoomDelay={1000}
                />
            </div>
        </div>
    </div>
);

