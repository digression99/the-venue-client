import React from 'react';
import classNames from 'classnames/bind';

import styles  from '../../resources/styles.css';
import { Description, Discount } from '../molecules';

const cx = classNames.bind(styles);

const Highlights = () => {
    return (
        <div
            className={cx('bck_white')}
            style={{
                padding : '2rem 0'
            }}
        >
            <div className={cx('highlight_wrapper')}>
                <Description />
                <Discount />
            </div>
        </div>
    );
};

export default Highlights;
