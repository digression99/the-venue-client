import React from 'react';
import classNames from 'classnames/bind';

import styles from '../../resources/styles.css';

const cx = classNames.bind(styles);

const Location = () => {
    return (
        <div className={cx('location_wrapper')}>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12661.0214751436!2d127.05449724108252!3d37.50189475009903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca4143ba5e3bf%3A0x3452c7b41400a0db!2z7Iqk7YOA67KF7IqkIO2PrOyKpOy9lOygkA!5e0!3m2!1sko!2skr!4v1539934662296"
                width="100%"
                height="500px"
                frameBorder="0"
                style={{border : 0}}
                allowFullScreen
            />
            <div className={cx('location_tag')}>
                <div>Location</div>
            </div>
        </div>
    );
};

export default Location;
