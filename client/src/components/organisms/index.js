import VenueInfo from './VenueInfo';
import Featured from './Featured';
import Header from './Header';
import Highlights from './Highlights';
import Pricing from './Pricing';
import Location from './Location';
import Footer from './Footer';

export {
    VenueInfo,
    Featured,
    Header,
    Highlights,
    Pricing,
    Location,
    Footer
};