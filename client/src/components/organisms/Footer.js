import React from 'react';
import classNames from 'classnames/bind';
import Fade from 'react-reveal/Fade';
import styles from '../../resources/styles.css';

const cx = classNames.bind(styles);

const Footer = () => {
    return (
        <footer className={cx('bck_red')}>
            <Fade
                delay={500}
            >
                <div className={cx('font_righteous', 'footer_logo_venue')}>
                    The Venue
                </div>
                <div className={cx('footer_copyright')}>
                    Pseudocoder Kim All rights reserved.
                </div>
            </Fade>
        </footer>
    );
};

export default Footer;
